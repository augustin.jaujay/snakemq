from enum import IntEnum

class Moves(IntEnum):
    MOVE_RIGHT = 0
    MOVE_LEFT = 1
    MOVE_UP = 2
    MOVE_DOWN = 3