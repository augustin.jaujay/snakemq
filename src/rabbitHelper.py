import pika
import uuid
from pika import exchange_type

def send(queue, message):
    connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
    channel = connection.channel()

    channel.exchange_declare(exchange=queue,
        exchange_type='fanout')

    channel.basic_publish(exchange=queue,
        routing_key = '',
        body = str(message))
    
    connection.close()

def receive(queue, callback):
    connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
    channel = connection.channel()

    channel.exchange_declare(exchange=queue,
        exchange_type='fanout')
    
    _uuid = str(uuid.uuid4())
    result = channel.queue_declare(exclusive=True, queue=_uuid)
    queue_name = result.method.queue

    channel.queue_bind(exchange=queue,
        queue=queue_name)
    
    channel.basic_consume(queue_name, callback, auto_ack=True)

    channel.start_consuming()