import pygame
import random
import _thread
from rabbitHelper import send, receive

class Block:
    def __init__(self, surface, size):
        self.surface = surface
        w, h = self.surface.get_size()
        self.size = size
        self.x = random.randint(1, w / self.size - 1) * self.size
        self.y = random.randint(1, h / self.size - 1) * self.size
        self.draw()
        _thread.start_new_thread(receive, ('headPos', self.monitorSnakePos))

    def draw(self):
        blockRect = pygame.Rect(self.x - self.size / 2, self.y - self.size / 2, self.size, self.size)
        pygame.draw.rect(self.surface, (0, 255, 0), blockRect)

    def monitorSnakePos(self, ch, method, properties, body):
        splitted = str(body).split("'")
        splitted = splitted[1].split("/")
        if int(float(splitted[0])) == self.x and int(float(splitted[1])) == self.y:
            send('blockCaught', str(self.x) + '/' + str(self.y))
            _thread.exit()